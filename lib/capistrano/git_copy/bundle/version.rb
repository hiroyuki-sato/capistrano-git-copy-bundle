# frozen_string_literal: true

module Capistrano
  module GitCopy
    module Bundle
      VERSION = '0.2.1'
    end
  end
end
