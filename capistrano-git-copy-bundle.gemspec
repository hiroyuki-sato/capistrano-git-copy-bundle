# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'capistrano/git_copy/bundle/version'

Gem::Specification.new do |spec|
  spec.name          = 'capistrano-git-copy-bundle'
  spec.version       = Capistrano::GitCopy::Bundle::VERSION
  spec.authors       = ['Florian Schwab']
  spec.email         = ['me@ydkn.de']
  spec.summary       = 'Packages gems locally and uploads them to the remote server.'
  spec.description   = 'Packages gems locally and uploads them to the remote server.'
  spec.homepage      = 'https://gitlab.com/ydkn/capistrano-git-copy-bundle'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'bundler',            '~> 2.0'
  spec.add_dependency 'capistrano-bundler', '~> 1.6'

  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-performance'
end
